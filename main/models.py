from django.db import models

from root.models import User, SimpleModel


class Licence(SimpleModel):
    code = models.CharField(max_length=255)

    def __str__(self):
        return self.code


class Language(SimpleModel):
    abbr = models.CharField(max_length=3)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    @classmethod
    def as_choices(cls):
        return cls.objects.all().values_list('id', 'name')


class Sentence(SimpleModel):
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    licence = models.ForeignKey(Licence, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    has_audio = models.BooleanField(default=False)
    text = models.TextField()
    remote_id = models.IntegerField()
    url = models.URLField()

    def __str__(self):
        return self.text


class UserTrial(SimpleModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    sentence = models.ForeignKey(Sentence, on_delete=models.CASCADE)
    trial = models.IntegerField()
    correct = models.BooleanField()


class UserLearning(SimpleModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
