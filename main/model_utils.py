from django.db import models

from root.exceptions import CustomAssertionError
from django.conf import settings
from main.models import Language, UserLearning

from root.models import ExtraAttrValue


def get_user_language(user):
    """
    Return user's current language and the language's current similarity matrix
    :param user:
    :return:
    """
    current_language_value = ExtraAttrValue.objects.filter(attr=settings.ATTRS.user.current_language, owner_id=user.id,
                                                           user=user).first()
    if current_language_value:
        current_language_id = current_language_value.value
        current_language = Language.objects.get(pk=current_language_id)

    else:
        current_language = UserLearning.objects.filter(user=user).first()
        if current_language is not None:
            current_language = current_language.language
            ExtraAttrValue.objects.create(attr=settings.ATTRS.user.current_language, owner_id=user.id, user=user,
                                          value=current_language.id)

    return current_language


def set_user_language(user, language):
    current_language_value = ExtraAttrValue.objects.filter(attr=settings.ATTRS.user.current_language, owner_id=user.id,
                                                           user=user).first()
    if current_language_value is None:
        ExtraAttrValue.objects.create(attr=settings.ATTRS.user.current_language, owner_id=user.id, user=user,
                                      value=language.id)
    else:
        current_language_value.value = str(language.id)
        current_language_value.save()


def assert_values(value, value_range):
    if value not in value_range:
        raise CustomAssertionError('Invalid value {}'.format(value))


def get_or_error(obj, key):
    """
    Get key or filter Model for given attributes. If None found, error
    :param obj: can be dict, Model class name, or a generic object
    :param key: can be a string or a dict containing query filters
    :return: the value or object if found
    """
    if isinstance(obj, dict):
        value = obj.get(key, None)
    elif issubclass(obj, models.Model):
        value = obj.objects.filter(**key).first()
    else:
        value = getattr(obj, key, None)
    if value is None:
        if isinstance(key, dict):
            error = 'No {} with {} exists'.format(
                obj.__name__.lower(), ', '.join(['{}={}'.format(k, v) for k, v in key.items()])
            )
            raise CustomAssertionError(error)
        raise CustomAssertionError('{} doesn\'t exist'.format(key))

    return value
