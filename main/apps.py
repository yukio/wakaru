import os

from django.apps import AppConfig
from django.conf import settings
from django.db import OperationalError
from django.db import ProgrammingError
from dotmap import DotMap


def get_builtin_attrs():
    """
    Query and store some built-in ExtraAttr in settings

    :return: None
    """
    from root.models import ExtraAttr, ValueTypes, User

    goc = ExtraAttr.objects.get_or_create

    current_language_attr, _ = goc(klass=User.__name__, name='current-language', type=ValueTypes.SHORT_TEXT)

    settings.ATTRS = DotMap(
        user=DotMap(current_language=current_language_attr)
    )


class Config(AppConfig):
    name = 'main'

    def ready(self):
        """
        Register app specific's views, request handlers and classes to the root app

        Note
        ---
        The app should only load when it runs as a server, not when the fixtures are being loaded,
        otherwise we end up with database problem.
        If the fixtures are loaded by the migrate.sh script (which they should be)
        then that script will set an environment variable IMPORTING_FIXTURE to "true"
        before it runs and to "false" when it finishes

        :return: None
        """
        is_importing_fixture = os.getenv('IMPORTING_FIXTURE', 'false') == 'true'

        if not is_importing_fixture:
            try:
                from root.models import User
                from root.views import register_app_modules, init_tables

                is_database_empty = User.objects.all().count() == 0

                if not is_database_empty:
                    register_app_modules(self.name, 'models')
                    register_app_modules(self.name, 'request_handlers.audio')
                    register_app_modules(self.name, 'request_handlers.answers')
                    register_app_modules('root', 'models')
                    register_app_modules(self.name, 'grid_getters')

                    get_builtin_attrs()
                    init_tables()

            except (ProgrammingError, OperationalError):
                pass
