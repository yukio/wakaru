import json

from django.http import HttpResponse
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.views.generic import TemplateView, FormView
from main.forms import ContactUsForm

from main.request_handlers.templates import populate_context
from root.models import User
from root.utils import SendEmailThread


def get_home_page(request):
    return redirect('show-sentence')

extra_context = {}


class ContactUsView(FormView):
    template_name = 'contact-us.html'
    page_name = 'contact-us'
    form_class = ContactUsForm

    def get_context_data(self, **kwargs):
        context = super(ContactUsView, self).get_context_data(**kwargs)
        populate_context(self, context)
        return context

    def form_invalid(self, form):
        context = self.get_context_data()
        rendered = render_to_string('partials/contact-us-form.html', context=context)

        return HttpResponse(json.dumps(dict(message=dict(success=False, html=rendered))))

    def form_valid(self, form, **kwargs):
        data = form.cleaned_data

        superuser = User.objects.get(username='superuser')

        subject = 'Someone just contacted Koe'
        template = 'contact-received'

        send_email_thread = SendEmailThread(subject, template, [superuser.email], context=data)
        send_email_thread.start()

        rendered = render_to_string('support-confirmation.html')
        return HttpResponse(json.dumps(dict(message=dict(success=True, html=rendered))))


def get_view(name):
    """
    Get a generic TemplateBased view that uses only common context
    :param name: name of the view. A `name`.html must exist in the template folder
    :return:
    """
    class View(TemplateView):
        page_name = name
        template_name = name + '.html'

        def get_context_data(self, **kwargs):
            context = super(View, self).get_context_data(**kwargs)
            populate_context(self, context)

            extra_context_func = extra_context.get(self.__class__.page_name, None)
            if callable(extra_context_func):
                extra_context_func(self.request, context)

            return context

    return View.as_view()
