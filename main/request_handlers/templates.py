from django.template.loader import render_to_string
from dotmap import DotMap

from main.model_utils import get_or_error, get_user_language, set_user_language
from main.models import UserLearning, Language

__all__ = ['get_sidebar']


def populate_context(obj, context):
    page_name = getattr(obj, 'page_name', None)
    if page_name is None:
        page_name = obj.__class__.page_name

    user = obj.request.user
    gets = obj.request.GET

    for key, value in gets.items():
        if key.startswith('__'):
            context['external{}'.format(key)] = value
        elif key.startswith('_'):
            context['internal{}'.format(key)] = value
        else:
            context[key] = value

    lang_ids = UserLearning.objects.filter(user=user).values_list('language', flat=True)
    languages = Language.objects.filter(id__in=lang_ids)
    language_name = gets.get('language', None)

    current_language = None
    if language_name:
        current_language = languages.filter(name=language_name).first()
        if current_language:
            set_user_language(user, current_language)
    if current_language is None:
        current_language = get_user_language(user)

    context['languages'] = languages
    context['current_language'] = current_language
    context['page'] = page_name


def get_sidebar(request):
    page = get_or_error(request.POST, 'page')
    context = dict(user=request.user, page=page)

    obj = DotMap(page_name=page, request=request)

    populate_context(obj, context)
    return render_to_string('sidebar/sidebar.html', context=context)
