import os
from urllib import request

from django.conf import settings
from main.model_utils import get_or_error
from main.models import Sentence

from root.utils import audio_path, ensure_parent_folder_exists

__all__ = ['get_audio_file_url']


def download(sentence, dest):
    sentence_url = 'https://audio.tatoeba.org/sentences/{}/{}.mp3'.format(sentence.language.abbr, sentence.remote_id)

    ensure_parent_folder_exists(dest)
    request.urlretrieve(sentence_url, dest)
    return True


def get_audio_file_url(request):
    sentence_id = get_or_error(request.POST, 'sentence-id')
    sentence = get_or_error(Sentence, dict(id=sentence_id))

    mp3_file = audio_path(sentence.id, settings.AUDIO_COMPRESSED_FORMAT)
    if not os.path.isfile(mp3_file):
        download(sentence, mp3_file)

    return audio_path(sentence.id, settings.AUDIO_COMPRESSED_FORMAT, for_url=True)
