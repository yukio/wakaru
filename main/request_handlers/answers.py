import sys
import unicodedata

from django.db.models import Q
from pykakasi import kakasi

from main.model_utils import get_or_error
from main.models import UserTrial, Sentence

__all__ = ['check_answer', 'get_new_trial', 'giveup']

time_between_run = 5
kak = kakasi()
kak.setMode('J', 'H')  # Japanese to hiragana
conv = kak.getConverter()


tbl = [chr(i) for i in range(sys.maxunicode) if unicodedata.category(chr(i))[0] in ['P', 'M', 'C']]
tbl = ''.join(tbl)
trans = str.maketrans('', '', tbl)


def clean_up(ori, lang):
    if lang == 'jpn':
        return conv.do(ori.translate(trans))
    else:
        return ori.translate(trans).lower()


def check_answer(request):
    answer = get_or_error(request.POST, 'answer')
    trial_id = get_or_error(request.POST, 'trial')
    trial = get_or_error(UserTrial, dict(id=trial_id))

    sentence = trial.sentence
    correct_answer = sentence.text

    clean_correct_answer = clean_up(correct_answer, sentence.language.abbr)
    clean_answer = clean_up(answer, sentence.language.abbr)

    if clean_correct_answer == clean_answer:
        trial.correct = True
    else:
        trial.trial += 1
    trial.save()

    trials_left = 3 - trial.trial

    return dict(correct=trial.correct, clean=clean_answer, seikai=clean_correct_answer, nokori=trials_left,
                original=correct_answer)


def giveup(request):
    current_trial = get_or_error(request.POST, 'trial')
    trial = get_or_error(UserTrial, dict(id=current_trial))
    trial.trial = 3
    trial.save()

    sentence = trial.sentence
    correct_answer = sentence.text

    return dict(original=correct_answer)


def get_new_trial(request):
    user = request.user
    language_id = get_or_error(request.POST, 'language')
    current_trial = request.POST.get('trial', None)
    if current_trial:
        trial = get_or_error(UserTrial, dict(id=current_trial))
        if not trial.correct:
            trial.trial = 3
            trial.save()

    trial = UserTrial.objects.filter(user=user, correct=False, trial__lt=3, sentence__language=language_id).first()
    if trial is None:
        # pick a new sentence
        # Exclude a sentence that have been trialed:
        trialed = UserTrial.objects.filter(user=user).filter(Q(trial__gte=3) | Q(correct=True)) \
            .values_list('sentence', flat=True)
        sentences = Sentence.objects.filter(has_audio=True).exclude(pk__in=trialed)
        sentence = sentences.filter(language__id=language_id).first()
        trial = UserTrial()
        trial.user = user
        trial.sentence = sentence
        trial.trial = 0
        trial.correct = False
        trial.save()

    sentence = trial.sentence
    trials_left = 3 - trial.trial
    retval = dict(trial=trial.id, nokori=trials_left, sentence=sentence.id)

    if trials_left == 0:
        correct_answer = sentence.text
        clean_correct_answer = clean_up(correct_answer, sentence.language.abbr)
        retval.original = clean_correct_answer

    return retval
