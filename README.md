Wakaru
===

## What is it?
Blah

[Install](docs/INSTALL.md)

[Run](docs/RUN.md)

[Upgrade](docs/UPGRADE.md)

[Deploy](docs/DEPLOY.md)

## Licence
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

> TL;DR    
>  - You are allowed to copy, use and distribute this work.    
>  - You are allowed to change the source code and distribute that under similar or entirely different licence.    

Read the full [LICENCE](LICENCE.md) here

Contact us if you want a different licensing option.

## Copyright
Copyright (c) 2013-9999 Yukio Fukuzawa. All rights reserved.
