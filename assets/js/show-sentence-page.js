require('bootstrap-slider/dist/bootstrap-slider.js');
const jsdiff = require('diff/dist/diff.min.js');

import {changePlaybackSpeed, queryAndHandleAudioGetOrPost} from './audio-handler';
import {Visualiser} from './audio-visualisation'
import {postRequest} from './ajax-handler';

const audioData = {};
const vizContainerId = '#track-visualisation';
const speedSlider = $('#speed-slider');
const answer = $('#answer');
const answerInput = answer.find('input');
const answerSubmitBtn = answer.find('button');

const language = $('#answer-and-result').attr('language');
const instructionText = $('.instruction-text');
const originalCorrect = $('.original-correct');
const trialLeft = $('.trials-left');

const nextSentenceBtn = $('#next-sentence-btn');
const giveupBtn = $('#give-up-btn');

const args = {};

const showCallout = function(calloutSelect) {
    $('.bs-callout').hide();
    $(calloutSelect).show();
};

const getNewTrial = function () {
    return new Promise(function (resolve) {
        postRequest({
            requestSlug: 'main/get-new-trial',
            data: {language, trial: args.trial},
            immediate: true,
            onSuccess({trial, nokori, sentence}) {
                args.trial = trial;
                args.nokori = nokori;
                args.sentence = sentence;

                trialLeft.html(nokori);
                answerInput.val('');
                giveupBtn.prop('disabled', false);
                nextSentenceBtn.prop('disabled', true);

                showCallout('#instruction');
                resolve();
            }
        });
    });
};

const giveUp = function () {
    return new Promise(function (resolve) {
        postRequest({
            requestSlug: 'main/giveup',
            data: {trial: args.trial},
            immediate: true,
            onSuccess({original}) {
                originalCorrect.html(`<span class="correct">${original}</span>`);
                showCallout('#result-givenup');
                giveupBtn.prop('disabled', true);
                nextSentenceBtn.prop('disabled', false);
                answerInput.val('');
                resolve();
            }
        });
    });
};


const showDiff = function (seikai, clean) {
    let diffFunc = jsdiff.diffWords;

    // Japanese and Chinese/Korean need to be compared by character.
    // Quick and dirty way to check if the language is CJK is to see if there is any space in the sentence
    if (seikai.indexOf(' ') === -1) {
        diffFunc = jsdiff.diffChars;
    }
    let diff = diffFunc(seikai, clean);
    instructionText.html('');

    diff.forEach(function (part) {
        let classes;
        let span = $('<span></span>');
        let text = part.value;
        if (part.removed == undefined && part.added === undefined) {
            classes = 'correct';
        }
        else if (part.added) {
            classes = 'incorrect';
        }
        else {
            classes = 'missing';
            text = '____';
        }
        if (classes) {
            span.addClass(classes);
            span.html(text);
            instructionText.append(span);
        }
    });
};


const initController = function () {
    speedSlider.slider();

    speedSlider.on('slideStop', function (slideEvt) {
        changePlaybackSpeed(slideEvt.value);
    });

    speedSlider.find('.slider').on('click', function () {
        let newValue = speedSlider.find('.tooltip-inner').text();
        changePlaybackSpeed(parseInt(newValue));
    });

    answerSubmitBtn.click(function (e) {
        e.preventDefault();
        let ans = answerInput.val();
        postRequest({
            requestSlug: 'main/check-answer',
            data: {
                answer: ans,
                trial: args.trial
            },
            onSuccess({correct, seikai, nokori, clean, original}) {
                if (correct) {
                    originalCorrect.html(`<span class="correct">${original}</span>`);
                    showCallout('#result-correct');

                    giveupBtn.prop('disabled', true);
                    nextSentenceBtn.prop('disabled', false);
                }
                else {
                    showDiff(seikai, clean);
                    trialLeft.html(nokori);

                    if (nokori === 0) {
                        originalCorrect.html(`<span class="correct">${original}</span>`);

                        showCallout('#result-no-more-trial');
                        giveupBtn.prop('disabled', true);
                        nextSentenceBtn.prop('disabled', false);
                    }
                    else {
                        showCallout('#result-incorrect');
                        giveupBtn.prop('disabled', false);
                        nextSentenceBtn.prop('disabled', true);
                    }
                }
            },
            immediate: true,
        });
    });

    nextSentenceBtn.click(function () {
        getNewTrial().then(loadSong).then(displaySong);
    });

    giveupBtn.click(function () {
        giveUp();
    })
};


export const preRun = function () {
    return Promise.resolve();
};


/**
 * Load an existing song into view by ID & pretend it is a raw audio
 * @returns {Promise}
 */
const loadSong = function () {
    let sentenceId = args.sentence;
    return new Promise(function (resolve) {
        postRequest({
            requestSlug: 'main/get-audio-file-url',
            data: {'sentence-id': sentenceId},
            immediate: true,
            onSuccess(fileUrl) {
                queryAndHandleAudioGetOrPost({
                    url: fileUrl,
                    cacheKey: sentenceId,
                    callback(sig_, fs_) {
                        resolve({sig_, fs_});
                    }
                });
            }
        });
    });
};

let colourMap = 'Green';
const spectViz = new Visualiser(vizContainerId);
spectViz.initScroll();
spectViz.initController();
spectViz.resetArgs({zoom: 100, contrast: 50, noverlap: 0, colourMap});


const displaySong = function ({sig_, fs_}) {
    audioData.sig = sig_;
    audioData.fs = fs_;
    audioData.length = sig_.length;
    audioData.durationMs = audioData.length * 1000 / fs_;

    spectViz.unsetData();
    spectViz.setData(audioData);
    spectViz.resetArgs({zoom: 1600});
    spectViz.initCanvas();
    spectViz.visualiseSpectrogram();
};


export const run = function () {
    return getNewTrial().then(loadSong).then(displaySong);
};


export const postRun = function () {
    initController();
    return Promise.resolve();
};
