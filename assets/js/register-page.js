import {initSelectizeSimple} from './selectize-formatter';

let selectizeArgs = {
    create: false,
    maxItems: 3
};

const initSelectize = function () {
    let languageSectionEl = $('#id_languages');
    initSelectizeSimple(languageSectionEl, undefined, selectizeArgs);
};


export const run = function () {
    initSelectize();
    return Promise.resolve();
};
