import {defaultGridOptions, FlexibleGrid} from './flexible-grid';
import {debug, deepCopy, setCache, getCache} from './utils';
require('bootstrap-slider/dist/bootstrap-slider.js');

const gridOptions = deepCopy(defaultGridOptions);
gridOptions.rowHeight = 30;


class Grid extends FlexibleGrid {
    init() {

        super.init({
            'grid-name': 'parents',
            'grid-type': 'parents-grid',
            'default-field': 'name',
            'import-key': 'email',
            gridOptions
        });
    }
}

export const grid = new Grid();
const gridStatus = $('#grid-status');
const gridStatusNSelected = gridStatus.find('#nselected');
const gridStatusNTotal = gridStatus.find('#ntotal');
const sendSmsBtn = $('#send-sms-btn');
const sendSmsModal = $('#send-sms-modal');

const resetStatus = function (e, args) {
    e.preventDefault();
    let nRowChanged = 0;
    if (e.type == 'row-added') {
        nRowChanged = 1;
    }
    else if (e.type == 'rows-added') {
        nRowChanged = args.rows.length;
    }
    else if (e.type == 'row-removed') {
        nRowChanged = -1;
    }
    else if (e.type == 'rows-removed') {
        nRowChanged = -args.rows.length;
    }
    let nSelectedRows = parseInt(gridStatusNSelected.html());
    gridStatusNSelected.html(nSelectedRows + nRowChanged);

    // Restore keyboard navigation to the grid
    $($('div[hidefocus]')[0]).focus();
};

/**
 * Subscribe to this instance of Flexible Grid. This must be called only once when the page loads
 */
const subscribeFlexibleEvents = function () {
    debug('subscribeFlexibleEvents called from parents-pages');

    grid.on('row-added', resetStatus);
    grid.on('rows-added', resetStatus);

    grid.on('row-removed', resetStatus);
    grid.on('rows-removed', resetStatus);
};


/**
 * Subscribe to events on the slick grid. This must be called everytime the slick is reconstructed, e.g. when changing
 * screen orientation or size
 */
const subscribeSlickEvents = function () {
    grid.subscribeDv('onRowCountChanged', function (e, args) {
        let currentRowCount = args.current;
        gridStatusNTotal.html(currentRowCount);
    });
};

/**
 * Set the focus on the grid right after page is loaded.
 * This is mainly so that user can use Page Up and Page Down right away
 */
const focusOnGridOnInit = function () {
    $($('div[hidefocus]')[0]).focus();
};


let extraArgs = {
};

let gridArgs = {
    multiSelect: true,
    doCacheSelectableOptions: false
};


export const preRun = function() {
    injectSelectableOptions();
    return Promise.resolve();
};


const injectSelectableOptions = function () {
    let constants = getCache('literals', 'CommunicationMethod');
    let options = [];
    $.each(constants, function (label, value) {
        options.push({
            label,
            value
        });
    });

    let selectizeOptions = {
        valueField: 'value',
        labelField: 'label',
        searchField: 'label',
        create: false,
        selectOnTab: false,
        openOnFocus: true,
        dropdownDirection: 'auto',
        render: {
            option (item) {
                return `<div>${item.label}</div>`;
            }
        },
    };

    let renderOptions = {
        options,
        selectizeOptions
    };

    setCache('selectableOptions', '__render-options__prefered_method', renderOptions);
};


export const run = function () {
    grid.init();

    return new Promise(function(resolve) {
        grid.initMainGridHeader(gridArgs, extraArgs).then(function () {
            subscribeSlickEvents();
            subscribeFlexibleEvents();

            grid.initMainGridContent(gridArgs, extraArgs).then(function() {
                focusOnGridOnInit();
                resolve();
            });
        });
    });
};

export const postRun = function () {
    sendSmsBtn.click(function () {
        sendSmsModal.modal('show');
    });

    return Promise.resolve();
};

export const viewPortChangeHandler = function () {
    grid.mainGrid.resizeCanvas();
};
