import {postRequest} from './ajax-handler';
const contactUsForm = $('#contact-us-form');
const saveTrackInfoBtn = contactUsForm.find('#submit-inquiry');


const initSaveTrackInfoBtn = function () {
    saveTrackInfoBtn.click(function () {
        contactUsForm.submit();
    });

    contactUsForm.submit(function (e) {
        e.preventDefault();
        let formData = new FormData(this);
        let url = this.getAttribute('url');

        postRequest({
            url,
            data: formData,
            onSuccess (response) {
                let formHtml = response.html;

                if (formHtml) {
                    contactUsForm.find('.replaceable').html(formHtml);
                }

                if (response.success) {
                    saveTrackInfoBtn.remove();
                }
            }
        });
        return false;
    });
};


export const run = function () {
    initSaveTrackInfoBtn();
    return Promise.resolve();
};
