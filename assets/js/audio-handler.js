const bufferToWav = require('audiobuffer-to-wav');
const resampler = require('audio-resampler');

import {isNull, noop} from './utils';
import {handleResponse, postRequest, createSpinner} from './ajax-handler';

/**
 * the global instance of AudioContext (we maintain only one instance at all time)
 * this instance must always be closed when not playing audio
 * @type {*}
 */
let audioContext = null;

/**
 * the global instance of AudioBuffer (we maintain only one instance at all time)
 * @type {null}
 */
let audioBuffer = null;
let cachedArrays = {};

/**
 * Playback speed, global
 */
let playbackSpeed = 100;

/**
 * Self-explanatory
 * @param newSpeed
 */
export const changePlaybackSpeed = function (newSpeed) {
    playbackSpeed = newSpeed;
};

/**
 * Make sure we have a suitable AudioContext class
 */
export const initAudioContext = function () {

    if (!window.AudioContext) {
        if (!window.webkitAudioContext) {
            alert('Your browser does not support any AudioContext and cannot play back this audio.');
            return;
        }
        window.AudioContext = window.webkitAudioContext;
    }

    audioContext = new AudioContext();
    audioContext.close();
};


/**
 * Plays a owner of the audio from begin point to end point
 * @param beginSec see below
 * @param endSec and beginMs : in seconds
 * @param onStartCallback callback to be called when the audio starts
 * @param onEndedCallback callback to be called when the audio finishes
 */
const playAudio = function ({beginSec = 'start', endSec = 'end', onStartCallback = null, onEndedCallback = null}) {
    if (isNull(audioContext) || audioContext.state === 'closed') {
        audioContext = new AudioContext();
    }

    /*
     * Prevent multiple audio playing at the same time: stop any instance if audioContext that is currently running
     */
    else if (!isNull(audioContext) && audioContext.state === 'running') {
        audioContext.close();
        audioContext = new AudioContext();
    }
    let source = audioContext.createBufferSource();
    source.buffer = audioBuffer;

    source.playbackRate.setValueAtTime(playbackSpeed / 100.0, 0);
    source.connect(audioContext.destination);

    if (beginSec === 'start') {
        beginSec = 0
    }

    if (endSec === 'end') {
        endSec = audioBuffer.duration;
    }

    // For more information, read up AudioBufferSourceNode.start([when][, offset][, duration])
    if (typeof onStartCallback === 'function') {
        onStartCallback(playbackSpeed);
    }

    source.start(0, beginSec, endSec - beginSec);
    source.onended = function () {
        audioContext.close();
        if (typeof onEndedCallback === 'function') {
            onEndedCallback();
        }
    };
};


/**
 * Stop the audio if it is playing
 */
export const stopAudio = function () {
    if (!isNull(audioContext) && audioContext.state === 'running') {
        audioContext.close();
        audioContext = new AudioContext();
    }
};


/**
 * Convert audio data to audio buffer and then play
 * @param sig a Float32Array object
 * @param fs sampling rate
 * @param playAudioArgs arguments to provide for playAudio
 */
export const playAudioDataArray = function (sig, fs, playAudioArgs) {
    audioBuffer = audioContext.createBuffer(1, sig.length, fs);
    audioBuffer.getChannelData(0).set(sig);
    playAudio(playAudioArgs);
};


/**
 * Convert Float32Array into audio object
 * @param sig a Float32Array object
 * @param fs sampling rate
 * @return Blob audio blob
 */
export const createAudioFromDataArray = function (sig, fs) {
    audioBuffer = audioContext.createBuffer(1, sig.length, fs);
    audioBuffer.getChannelData(0).set(sig);
    let wav = bufferToWav(audioBuffer);
    return new window.Blob([new DataView(wav)], {type: 'audio/wav'});
};


/**
 * Convert a dict to a FormData object, with the same key-value pairs
 * @param data
 * @returns {FormData}
 */
const convertToFormData = function (data) {
    let formData;
    if (data instanceof FormData) {
        formData = data;
    }
    else {
        formData = new FormData();
        for (let key in data) {
            if (Object.prototype.hasOwnProperty.call(data, key)) {
                formData.append(key, data[key]);
            }
        }
    }
    return formData;
};


const MIN_SAMPLE_RATE = 8000;
const MAX_SAMPLE_RATE = 96000;

/**
 * Browser might support broader range, but the official must-support is 8000-96000
 * So we resample if it falls out of this range for browser compatibility
 * @param _audioBuffer
 * @returns {*} A promise that resolves to a (resampled) audio buffer
 */
function resampleIfNecessary(_audioBuffer) {

    /**
     * Shortcut to create a promise that resamples
     * @param input original audiobuffer
     * @param newFs new sample rate
     * @returns {Promise} resolves to an audiobuffer. If already in range the original AB is returned
     */
    function resample(input, newFs) {
        return new Promise(function (resolve) {
            resampler(input, newFs, function (obj) {
                let ab = obj.getAudioBuffer();
                resolve(ab);
            });
        })
    }

    let fs = _audioBuffer.sampleRate;

    if (fs < MIN_SAMPLE_RATE) {
        return resample(_audioBuffer, MIN_SAMPLE_RATE);
    }
    else if (fs > MAX_SAMPLE_RATE) {
        return resample(_audioBuffer, MAX_SAMPLE_RATE);
    }
    else {
        return new Promise(function (resolve) {
            resolve(_audioBuffer);
        });
    }
}


/**
 * Query, cache a piece of audio, then provide the signal and fs as arguments to the callback function
 * @param url POST or GET url, depending on the existence of postData
 * @param formData if querying for segment, formData must contain segment-id
 * @param cacheKey key to persist this song/segment in the cache
 * @param callback
 */
export const queryAndHandleAudioGetOrPost = function ({url, cacheKey, formData, callback}) {
    const reader = new FileReader();
    reader.onload = function () {
        const arrayBuffer = reader.result;
        audioContext.decodeAudioData(arrayBuffer, function (_audioBuffer) {
            resampleIfNecessary(_audioBuffer).then(function (ab) {

                let fullAudioDataArray = ab.getChannelData(0);
                let sampleRate = ab.sampleRate;


                if (cacheKey && !window.noCache) {
                    cachedArrays[cacheKey] = [fullAudioDataArray, sampleRate];
                }
                callback(fullAudioDataArray, sampleRate);
            })
        });
    };
    let method = isNull(formData) ? 'GET' : 'POST';

    const xhr = new XMLHttpRequest();
    xhr.open(method, url, true);

    let spinner = createSpinner();

    xhr.onloadstart = spinner.start;
    xhr.onloadend = spinner.clear;

    // We expect the response to be audio/mp4 when success, and text when failure.
    // So we need this to change the response type accordingly
    xhr.onreadystatechange = function () {
        // When request finishes, handle success/failure according to the status
        if (this.readyState == 4) {
            if (this.status == 200) {
                reader.readAsArrayBuffer(this.response);
            }
            else {
                handleResponse({
                    response: this.responseText,
                    isSuccess: false,
                })

            }
        }
        // When request is received, check if it is successful/failed and set the response type
        else if (this.readyState == 2) {
            if (this.status == 200) {
                this.responseType = 'blob';
            }
            else {
                this.responseType = 'text';
            }
        }
    };

    $.event.trigger('ajaxStart');
    xhr.send(formData);
};


/**
 * Query, cache a piece of audio, then provide the signal and fs as arguments to the callback function
 * @param url POST or GET url, depending on the existence of postData
 * @param postData POST data that contains the id of the song/segment to be downloaded. null to use GET
 * @param cacheKey key to persist this song/segment in the cache
 * @param callback
 */
export const queryAndHandleAudio = function ({url, cacheKey, postData}, callback) {
    let cached = cachedArrays[cacheKey];
    if (cacheKey && cached) {
        callback(cached[0], cached[1]);
    }
    else {
        let formData, fileId;
        if (!isNull(postData)) {
            fileId = postData['file-id'];
            formData = convertToFormData(postData);
        }
        if (fileId) {
            let onSuccess = function (fileUrl) {
                queryAndHandleAudioGetOrPost({
                    url: fileUrl,
                    cacheKey,
                    callback
                });
            };
            postRequest({
                requestSlug: 'koe/get-audio-file-url',
                data: postData,
                onSuccess,
                immediate: true,
            });
        }
        else {
            queryAndHandleAudioGetOrPost({
                url,
                cacheKey,
                callback,
                formData
            });
        }
    }
};

/**
 * Shortcut to download, cache a song/segment and them play for the specified segment
 * @param url POST url
 * @param postData POST data that contains the id of the song/segment to be downloaded
 * @param cacheKey key to persist this song/segment in the cache
 * @param playAudioArgs arguments to provide for playAudio
 */
export const queryAndPlayAudio = function ({url, postData, cacheKey, playAudioArgs = {}}) {
    let args = {
        url,
        cacheKey,
        postData
    };
    queryAndHandleAudio(args, function (sig, fs) {
        playAudioDataArray(sig, fs, playAudioArgs);
    });
};

/**
 * Upload an audio file and extract its data for in-browser processing
 *
 * @param file a file Blob
 * @param reader A FileReader instance
 * @param onProgress callback onprogress
 * @param onAbort callback onabort
 * @param onError callback onerror
 * @param onLoadStart callback onloadstart
 * @param onLoad callback onload
 * @return Promise that resolves to {sig, fs}
 */
export const loadLocalAudioFile = function ({
    file, reader = new FileReader(), onProgress = noop, onAbort = noop, onError = noop, onLoadStart = noop,
    onLoad = noop
}) {
    return new Promise(function (resolve) {
        reader.onload = function (e) {
            onLoad(e);
            const arrayBuffer = reader.result;
            audioContext.decodeAudioData(arrayBuffer, function (_audioBuffer) {
                let fullAudioDataArray = _audioBuffer.getChannelData(0);
                let sampleRate = _audioBuffer.sampleRate;
                resolve({
                    sig: fullAudioDataArray,
                    fs: sampleRate
                });
            });
        };
        reader.onprogress = onProgress;
        reader.onerror = onError;
        reader.onabort = onAbort;
        reader.onloadstart = onLoadStart;

        reader.readAsArrayBuffer(file);
    });
};
